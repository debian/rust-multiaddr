rust-multiaddr (0.18.2-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * autopkgtest-depend on dh-rust (not dh-cargo)
  * reduce autopkgtest to check single-feature tests only on amd64
  * stop mention dh-cargo in long description
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + improve usage comment
  * update watch file:
    + improve filename mangling
    + use Github API
  * update copyright info: update coverage
  * drop patch 1001, obsoleted by upstream changes;
    tighten (build-)dependency for crate unsigned-varint
  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Feb 2025 12:49:45 +0100

rust-multiaddr (0.18.1-5) unstable; urgency=medium

  * fix: set buildsystem=rust in overridden debhelper target

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 17:27:00 +0200

rust-multiaddr (0.18.1-4) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 14:47:00 +0200

rust-multiaddr (0.18.1-3) unstable; urgency=medium

  * fix autopkgtest-dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 26 May 2024 00:19:10 +0200

rust-multiaddr (0.18.1-2) unstable; urgency=medium

  * update patch
  * relax (build-)dependencies for crate unsigned-varint
  * update dh-cargo fork
  * update copyright info:
    + fix file path
    + update coverage
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 17 May 2024 20:27:31 +0200

rust-multiaddr (0.18.1-1) experimental; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump project version in virtual packages and autopkgtests
  * update dh-cargo fork
  * add patch 2001
    to cover newer releases of crate unsigned-varint;
    bump (build-)dependency for crate unsigned-varint

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 24 Nov 2023 09:21:11 +0100

rust-multiaddr (0.18.0-5) experimental; urgency=medium

  * drop patches 2001_*, obsoleted by Debian package updates;
    tighten (build-)dependency for crate multibase;
    (build-)depend on package for crate multihash
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 16 Sep 2023 19:36:24 +0200

rust-multiaddr (0.18.0-4) experimental; urgency=medium

  * update patches;
    tighten (build-)dependency for crate unsigned-varint

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 31 Aug 2023 15:14:21 +0200

rust-multiaddr (0.18.0-3) experimental; urgency=medium

  * adjust dependency for crate unsigned-varint;
    temporarily stop (build-)depend on package for crate multihash

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Aug 2023 16:03:38 +0200

rust-multiaddr (0.18.0-2) experimental; urgency=medium

  * fix: lower dependencies for crates multibase multihash

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 25 Jul 2023 09:16:37 +0200

rust-multiaddr (0.18.0-1) experimental; urgency=medium

  * initial packaging release;
    closes: bug#1041880

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 24 Jul 2023 22:58:17 +0200
